
var app = angular.module('app', ['ui.router']);

app.filter('removeSpacesThenLowercase', function () {
    return function (text) {
        var str = text.replace(/\s+/g, '');
        
        return str.toLowerCase();
    };
});

app.factory('sharedDataFt', function($rootScope) {
    var sharedData = {};

    sharedData.d = '';

    sharedData.prepForBroadcast = function(d, broadcast) {
        this.d = d;
        this.broadcastItem(broadcast);
    };

    sharedData.broadcastItem = function(broadcast) {
        $rootScope.$broadcast(broadcast);
    };

    sharedData.resetItem = function() {
        delete sharedData.d;
    }

    return sharedData;
});

app.controller('homeController', function ($scope, $state, $http, sharedDataFt) {
    $scope.sharedDataFt = sharedDataFt
    $scope.home = new Home($scope.sharedDataFt);
    $scope.total = 0;

    $scope.sizeOf = function(obj) {
        return Object.keys(obj).length;
    };

    $scope.submitTotal = function(total, totalVariable) {
        return $scope[totalVariable] = total;
    };

    $scope.showContent = function(event, input) {
        var $this = $(event.target);

        if (input == "checkbox") {
                var target = $this.parent().parent().prev().attr("href")
                , $totalInput =  $this.parent().parent().next().find(">div");

            if ($this.prop("checked")) {
                $(target).collapse('show'); 
                $totalInput.removeClass("ng-hide");
            } else {
                $(target).collapse('hide'); 
                $totalInput.addClass("ng-hide");
            };
        } else {
            var $inputTarget = $this.next().next()
                , $checkboxTarget = $this.next().find("input");

            if ($checkboxTarget.prop("checked")) {
                $checkboxTarget.prop("checked",false);
            } else {
                $checkboxTarget.prop("checked",true);
            };

            $.each($inputTarget.children(), function (i, v) {
                 if ($(v).hasClass("ng-hide")) {
                    $(v).removeClass("ng-hide");   
                } else {
                    $(v).addClass("ng-hide");
                };
            });
        };

        $scope.home.activateSubmit();
        return true;
    }

    $scope.showLocation = function(event) {
        var $this = $(event.target);

        if ($this.attr("target") != "") {
            var $next = $("." + $this.attr("target"))
                , flag = true;;

            while (flag) {
                var newtarget = "." + $next.attr("target");
                $next.prop("disabled", true);
                $next.prop('selectedIndex',0); 
                
                if (newtarget == ".") {
                  flag = false;  
                  continue;
                };
               
               $next = $(newtarget);
            }
        };

        $this.prop('selectedIndex',0); 
        $scope.home.activateSubmit();

        $this.unbind("change");
        $this.on("change", function() {
            $scope.home.activateSubmit();

            if (Number($(this).val()) != -1) {
                if ($this.attr("target") != "") {
                    var $next = $("." + $this.attr("target"));
                
                    $next.prop("disabled", false); 
                    $next.find("option").hide();
                    $($next.find("option")[0]).show();
                    $next.find("option[value^='" + $this.val() + "-']").show();
                };
            };    
        });
    }

    $scope.home.getList($scope, $http, "app/assets/json/form.json", function () {
        if (typeof $scope.form != 'object') {
            throw "The response must be a properly formatted object";
        };
    });
});

app.controller('bonusMoneyController', function ($scope, $state, sharedDataFt) {
    $scope.sharedDataFt = sharedDataFt
    $scope.data = sharedDataFt.d;
    $scope.bonusMoney = new BonusMoney($scope.sharedDataFt);

    if (sharedDataFt.d == "" || typeof sharedDataFt.d == "undefined") {
         window.location.href = "#/";
    };

    sharedDataFt.resetItem();
});

app.controller('policyController', function ($scope, $state, sharedDataFt) {
    $scope.data = sharedDataFt.d;
    $scope.policy = new Policy($scope.sharedDataFt);

    if (sharedDataFt.d == "" || typeof sharedDataFt.d == "undefined") {
         window.location.href = "#/";
    };

    sharedDataFt.resetItem();
});