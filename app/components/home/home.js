function Home(sharedDataFt) {
    this.sharedDataFt = sharedDataFt;
    console.log("home class");    
}

Home.prototype.getList = function ($scope, $http, url, callBack) {
    $http.get(url)
    .success(function (data) {
        for (var i = 0; i < Object.keys(data.collapse).length; i++) {
             $scope[Object.keys(data.collapse)[i]] = 0;
        }; 

        $scope.form = data;
        callBack();
    })
    .error(function (data, status) {
        throw 'Repos error' + status;
    });
}

Home.prototype.calculateTotal = function (totalVariable, total, last, num, event, callback) {
    if (event.keyCode == 8) { 
        if (num == null) {
            callback(total - last, totalVariable);
        } else {
            callback(total + (num - last), totalVariable);  
        };
    } else {
        if (total == 0 || (typeof last == "undefined")) {
            callback(total + num, totalVariable);
        } else {
            callback(total - last + num, totalVariable);
        };
    };
}

Home.prototype.submit = function($scope) {
    var $form = $("form")
        , $collapse = $form.find(".panel-group")
        , $collapseSelected = $collapse.find(".panel-heading").find("input[type='checkbox']:checked")
        , $localBlock = $form.find(".row:first-child + div").find(".form-group")
        , data = [];

    $.each($collapseSelected, function (i, v) {
        var $target = $(v).parent().parent().prev()
            , $extraData = $($target.attr("href")).find("input[type='checkbox']:checked")
            , newdata = {};

        target = $target.attr("href").replace('#','');

        if ($scope[target] > 0) {
            if ($extraData.length > 0) {
                newdata.extraData = [];
                $.each($extraData, function (i, v) {
                    newdata.extraData[i] = $(v).val();
                });           
            };

            newdata.title = $target.text().trim();
            newdata.value = $scope[target];

            data.push(newdata);
        } else {
            alert("values cant be 0")
        };
    });

    data.local = [];
    $.each($localBlock, function (i, v) {
        var $select = $(v).find("select");

        if ($select.val() != -1) {
            data.local[i] = []; 
            data.local[i]["name"] = $(v).find("select").attr("name");
            data.local[i]["value"] = $select.find("option:selected").text().trim();
        };
    });

    $scope.sharedDataFt.prepForBroadcast(data, "bonus_money");
    window.location.href = "#/bonus_money";
}

Home.prototype.activateSubmit = function() {
    var $require = $(".required")
        , checkbox = 0
        , select = 0
        , actives = 0;

    $.each($require, function (i, v) {
        if ($(v).attr("type") == "checkbox") {
            var $parent = $(v).parent().parent()
                , $inputVal = $parent.next().find("input[type='text']");

            if ($(v).is(':checked')) {
                checkbox++;
            };
        } else {
            if ($(v).val() != -1) {
                actives++;
            };
            select++;
        };
    });    


    var $form = $("form")
        , $submit = $form.find("input[type='submit']");

    if (checkbox > 0 && actives == select) {
        $submit.prop("disabled", false);
    } else {
        $submit.prop("disabled", true);
    };
}