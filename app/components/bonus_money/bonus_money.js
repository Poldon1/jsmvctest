function BonusMoney(sharedDataFt) {
    this.sharedDataFt = sharedDataFt;
    console.log("bonus money risk class");    
}

BonusMoney.prototype.quote = function ($scope, data) {
    var $resultTable = $(".result-table")
        , $rows = $resultTable.find("tr")
        , total = 0;

    $.each(data, function (i, v) {
        var $row = $($rows[i])
            , valuePerBonus = v.value;

        if (v.extraData) {
            valuePerBonus = valuePerBonus + ((valuePerBonus * v.extraData.length)/100)
        };

        $row.find("td:last-child").text("$" + valuePerBonus);
        total = total + v.value;
    });
    
    if (data[data.length - 1].value != -1) {
        total = total + ((total * 3)/100); 
    };

    $scope.total = total;

    $("#submit").prop("disabled", false);
}

BonusMoney.prototype.saveQuote = function ($scope ,data) {
    var newData = data;
    newData["total"] = $scope.total;

    $scope.sharedDataFt.prepForBroadcast(newData, "policy");
    window.location.href = "#/policy";
}