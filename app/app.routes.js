app.config(['$stateProvider', '$urlRouterProvider', 
    function($stateProvider, $urlRouterProvider) {
        var componentsRoute = 'app/components'; 

        $urlRouterProvider.otherwise('/');
        $stateProvider
        .state('index', {
            name: 'index',
            url: '/',
            views: {
                '': { 
                  templateUrl: componentsRoute + '/home/home.html'
                }
            }
        })

        .state('bonus_money', {
            name: 'bonus_money',
            url: '/bonus_money',
            views: {
                '': { 
                  templateUrl: componentsRoute + '/bonus_money/bonus_money.html'
                }
            }
        })

        .state('policy', {
            name: 'policy',
            url: '/policy',
            views: {
                '': { 
                  templateUrl: componentsRoute + '/policy/policy.html'
                }
            }
        })
    }
]);