# Cree un proyecto web implementando la funcionalidad propuesta en el wireframe.#
## Instrucciones:


1. Utilice AngularJS 1.X como framework MVC del proyecto.
2. Puede apoyarse de librerías como JQuery para las transiciones.
3. Utiliza JSON para la carga de listas. Éstas pueden provenir de archivos de texto
Puede utilizar nodejs si requiere emular llamadas a servicios.
4. Debe definir un patrón de desarrollo a utilizar en todo el proyecto.

## For Run:

1. you need node.js.
2. run npm install live-server.
3. run npm install for update dependencies.
4. type live-server for test.